# Assessment: Flexbox Cards

To begin, **fork and clone** the initial files from [this repository](https://github.com/KenzieAcademy/assessment-flexbox-cards).

The included cards.css stylesheet is missing all the flexbox properties needed to create the layout shown below:

![card-layout.png](https://i.snag.gy/bfi4Zp.jpg)

When you examine cards.css, you will find comments indicating which flex properties are missing. Please fill in these properties until you have recreated the layout shown above.

If you are having trouble remembering the behavior of the various flexbox properties, you may find it helpful to refer to the diagrams in [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/).

## HINTS

1. Look into using [flex-basis](https://mastery.games/post/the-difference-between-width-and-flex-basis/) on an item inside a flex container to set that item's initial size.
2. Look into using [flex-grow](https://css-tricks.com/almanac/properties/f/flex-grow/) on an item inside a flex container to allow that item to grow in size, if necessary.

### Submission
Commit your changes and push your code into your GitGHub repository. In Canvas, please submit your
GitHub url.
